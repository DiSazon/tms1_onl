from collections import Counter
text = "Me name is Dina. I have 1 cat. His name Kasper!"
text_low = text.lower()
lst_1 = list(text_low)
lst_2 = list()
for i in lst_1:
    if i == ' ' or i == '.' or i == '!' or i == ',' or i == ':' or i == '?':
        continue
    else:
        lst_2.append(i)
        lst_2.sort()
str_1 = (''.join(lst_2))
str_2 = Counter(str_1)
str_3 = str_2.most_common(1)
print(f"the most common letter and its amount in the text: {str_3}")
